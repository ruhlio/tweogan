#[derive(Debug, PartialEq, Eq)]
pub struct Value {
    data: Vec<u8>,
}

impl Value {
    pub fn new(data: &[u8]) -> Option<Value> {
        encode(data).map(|d| Value { data: d })
    }
}

fn encode(data: &[u8]) -> Option<Vec<u8>> {
    let len = data.len();
    if 0 == len {
        return Some(Vec::new());
    }
    if 1 == len && data[0] < 0x7f {
        return Some(vec![data[0]]);
    }
    else if len < 55 {
        let mut encoded = Vec::with_capacity(len + 2);
        encoded.push(0x80 + len as u8);
        encoded.push_all(data);
        return Some(encoded);
    }
    else if len < u32::max_value() as usize {
        let mut encoded = Vec::with_capacity(len + 8);
        print!("len = {}\n", len);
        let encoded_len = u32_to_bytes(len as u32);
        encoded.push(0xb7 + encoded_len.len() as u8);
        encoded.push_all(&encoded_len);
        encoded.push_all(data);
        return Some(encoded);
    }
    else {
        return None;
    }
}

pub fn u32_to_bytes(number: u32) -> Vec<u8> {
    if number <= 0xff {
        vec![number as u8]
    }
    else if number < 0xffff {
        vec![(number >> 8) as u8, number as u8]
    }
    else if number < 0xffffff {
        vec![(number >> 16) as u8, (number >> 8) as u8, number as u8]
    }
    else {
        vec![(number >> 24) as u8, (number >> 16) as u8, (number >> 8) as u8, number as u8]
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn u32_to_big_endian_bytes() {
        assert_eq!(u32_to_bytes(0xff), vec![0xff]);
        assert_eq!(u32_to_bytes(0xfeff), vec![0xfe, 0xff]);
        assert_eq!(u32_to_bytes(0xfdfeff), vec![0xfd, 0xfe, 0xff]);
        assert_eq!(u32_to_bytes(0xfcfdfeff), vec![0xfc, 0xfd, 0xfe, 0xff]);
    }

    #[test]
    fn empty_value() {
        let value = Value::new(&Vec::new());
        assert_eq!(value, Some(Value { data: Vec::new() }));
    }

    #[test]
    fn single_byte_value() {
        let value = Value::new(&vec![0x65]);
        assert_eq!(value, Some(Value { data: vec![0x65] }));
    }

    #[test]
    fn multi_byte_value_single_byte_header() {
        let value = Value::new(b"imma mean machine");
        let mut expected_data = vec![0x91];
        expected_data.push_all(b"imma mean machine");
        assert_eq!(value, Some(Value { data: expected_data }));
    }

    #[test]
    fn multi_byte_value_multi_byte_header() {
        let data = b"i could smell the death on his breath. his time had come. as his chest heaved shallow all the futility of life burst forth within his head. had he water to spare rivers would have run heavy down his face, yet instead his eyes twitched and shriveled back into their womb.";
        let value = Value::new(data);
        let mut expected_data = vec![0xb9, 0x01, 0x0e];
        expected_data.push_all(data);
        assert_eq!(value, Some(Value { data: expected_data }));
    }

    //todo: need more efficient way of testing this
    //#[test]
    fn value_too_long() {
        let size = u32::max_value() as usize + 1;
        let mut data = Vec::with_capacity(size);
        unsafe { data.set_len(size); }
        let value = Value::new(&data);
        assert_eq!(value, None);
    }

}

